import React from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import TodoInput from "./components/TodoInput";
import Tasks from "./components/Tasks";

const client = new ApolloClient({
    uri: "https://hasura-project-app.hasura.app/v1/graphql",
    cache: new InMemoryCache(),
});
function App() {
    return (
        <ApolloProvider client={client}>
            <div className="App mt-5">
                <TodoInput />
                <Tasks />
            </div>
        </ApolloProvider>
    );
}
export default App;
